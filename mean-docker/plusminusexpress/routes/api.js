const mongoose = require('mongoose'),
    autoIncrement = require('mongoose-auto-increment'),
    morgan = require('morgan'),
    bodyParser = require('body-parser');
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

mongoose.Promise = global.Promise;

// MongoDB URL from the docker-compose file
const dbHost = 'mongodb://database/mean-docker';

// Connect to mongodb
const connection = mongoose.connect(dbHost);
autoIncrement.initialize(connection);

// create mongoose schemas
const userSchema = new mongoose.Schema({
    email: String,
    firstName: String,
    lastName: String,
    password: String,
    token: String,
    created: {type: Date, default: Date.now}
});
userSchema.plugin(autoIncrement.plugin, {
    model: 'User',
    startAt: 1,
    incrementBy: 1
});

// create mongoose model
const User = mongoose.model('User', userSchema);


const activitySchema = new mongoose.Schema({
    userId: Number,
    dateTime: {type: Date},
    description: String,
    weightValue: Number,
    syncedDateTime: {type: Date, default: Date.now}
});
activitySchema.plugin(autoIncrement.plugin, {
    model: 'Activity',
    startAt: 1,
    incrementBy: 1
});

// create mongoose model
const Activity = mongoose.model('Activity', activitySchema);


const weightSchema = new mongoose.Schema({
    userId: Number,
    dateTime: {type: Date},
    weight: Number,
    syncedDateTime: {type: Date, default: Date.now}
});
weightSchema.plugin(autoIncrement.plugin, {
    model: 'Weight',
    startAt: 1,
    incrementBy: 1
});
const Weight = mongoose.model('Weight', weightSchema);

var validatedUser = null;

const nonValidatedPaths = ['/', '/authenticate'/*, '/users'*/];

function confirmValidatedUser(res, userId) {
  if (userId && validatedUser && userId == validatedUser._id) {
    return true;
  } else {
    res.sendStatus(403);
    return false;
  }
}

function generateNewUserToken(user) {
  var secureRandom = require('secure-random');
  var signingKey = secureRandom(256, {type: 'Buffer'});
  var token = jwt.sign(user._id, signingKey);
  console.log("New Token: "+ token);
  return token;
}

function saveUserWithNewToken(user) {
  user.token = generateNewUserToken(user);
  user.save(function(error, savedUser) {
    if (error) {
      console.log(error);
    } else {
      return savedUser;
    }
  });
  return user;
}

function setAuthorizationHeader(response, token) {
  response.set("Authorization", token);
}

router.use(function(request, response, next) {
  if(request.method == "OPTIONS" || nonValidatedPaths.indexOf(request.path) >= 0) {
    next();
  } else {
    validatedUser = null;
    var bearerToken;
    var bearerHeader = request.headers["authorization"];
    if (bearerHeader) {
      var bearer = bearerHeader.split(" ");
      bearerToken = bearer[1];
      request.token = bearerToken;
      console.log(request.token);
      User.findOne({token: request.token}, '-password', function (err, user) {
        if (user) {
          validatedUser = saveUserWithNewToken(user);
          setAuthorizationHeader(response, validatedUser.token);
        } else {
          // Token not found in database
          response.sendStatus(403);
          return response.send();
        }
        next();
      });
    } else {
      response.sendStatus(403);
      return response.send();
    }
  }
});

/* GET api listing. */
router.get('/', (req, res, next) => {
    res.send('API works');
    next();
});

router.post('/authenticate', (req, res, next) => {
  User.findOne({email: req.body.email, password: req.body.password}, '-password', function (err, user) {
    if (err) {
      res.json({
        type: false,
        data: "Error occurred: "+ err
      });
      next();
    } else {
      if (user) {
        validatedUser = user;
        setAuthorizationHeader(res, validatedUser.token);
        res.json({
          type: true,
          data: user,
          token: user.token
        });
        next();
      } else {
        res.json({
          type: false,
          data: "Incorrect email/password"
        });
        next();
      }
    }
  });
});

/* GET all users. */
router.get('/users', (req, res, next) => {
    User.find({ }, '-password', (err, users) => {
        if (err) {
          res.status(500).send(error)
        } else {
          res.status(200).json(users);
        }
        next();
    });
});

/* GET one user. */
router.get('/users/:id', (req, res, next) => {
  if (confirmValidatedUser(res, req.param.id)) {
    User.findById(req.param.id, '-password', (err, users) => {
        if (err) {
          res.status(500).send(error)
        } else {
          res.status(200).json(users);
        }
        next();
    });
  }
});

/* Create a user. */
router.post('/users', (req, res, next) => {
    let userModel = new User({
        email: req.body.email,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        password: req.body.password
    });

    userModel.save(function(error, user) {
        if (error) {
          res.status(500).send(error);
        } else {
          savedUser = saveUserWithNewToken(user);
          res.status(201).json({
                success: true,
                data: savedUser,
                token: savedUser.token
          });
        }
        next();
    });
});

/* GET all activities */
router.get('/activities', (req, res, next) => {
    Activity.find({}, (err, activities) => {
        if (err) {
          res.status(500).send(error);
        } else {
          res.status(200).json(activities);
        }
        next();
    });
});

// Get all the activities for a provided user
router.get('/activities/user/:userId', (req, res, next) => {
  if (confirmValidatedUser(res, req.params.userId)) {
    Activity.find({ userId: req.params.userId }, function (err, activities) {
        if(err) {
          res.status(500).send(err);
        } else {
          res.status(200).json(activities);
        }
        next();
    });
  }
});

/* Create an activity */
router.post('/activities', (req, res, next) => {
  if(confirmValidatedUser(res, req.body.userId)) {
      let activity = new Activity({
          userId: req.body.userId,
          dateTime: req.body.dateTime,
          description: req.body.description,
          weightValue: req.body.weightValue
      });

      activity.save(error => {
          if (error) {
            res.status(500).send(error);
          } else {
            res.status(201).json({
                message: 'Activity created successfully'
            });
          }
          next();
      });
    }
});


/* GET all user weights */
router.get('/weights', (req, res, next) => {
    Weight.find({}, (err, weights) => {
        if (err) {
          res.status(500).send(error);
        } else {
          res.status(200).json(weights);
        }
    });
});

/**
 * Get all of the weights for a specifc user
 */
router.get('/weights/user/:userId', (req, res, next) => {
  if (confirmValidatedUser(res, req.params.userId)) {
    Weight.find({ userId: req.params.userId }, function (err, weights) {
        if (err) {
          res.status(500).send(err)
        } else {
          res.status(200).json(weights);
        }
        next();
    });
  }
});

/* Create a weight */
router.post('/weights', (req, res, next) => {
  if (confirmValidatedUser(res, req.body.userId)) {
    let weight = new Weight({
        userId: req.body.userId,
        dateTime: req.body.dateTime,
        weight: req.body.weight
    });

    weight.save(error => {
        if (error) {
          res.status(500).send(error);
        } else {
          res.status(200).json({
              message: 'Weight created successfully'
          });
        }
        next();
    });
  }
});

router.post('/settings', (req, res, next) => {
    switch(req.body.action) {
        case 'delete':
            switch(req.body.fieldName) {
                case 'weight':
                    Weight.findById(req.body.fieldId, (err, weight) => {
                        if (err) {
                          res.status(500).send(err);
                        } else {
                          if(confirmValidatedUser(res, weight.userId)) {
                            weight.remove();
                            res.status(200).json({
                                fieldName: req.body.fieldName,
                                fieldId: req.body.fieldId
                            });
                          }
                        }
                    });
                    break;
                case 'activity':
                    Activity.findById(req.body.fieldId, (err, activity) => {
                        if (err) {
                          res.status(500).send(err);
                        } else {
                          if(confirmValidatedUser(res, activity.userId)) {
                            activity.remove();
                            res.status(200).json({
                                fieldName: req.body.fieldName,
                                fieldId: req.body.fieldId
                            });
                          }
                        }
                    });
                    break;
                case 'person':
                    User.findById(req.body.fieldId, (err, user) => {
                        if (err) {
                          res.status(500).send(err)
                        } else {
                          // Should be checking to see if it's an admin before allowing user to be deleted
                          user.remove()
                          Weight.deleteMany({userId: req.body.fieldId}, function (err) {});
                          Activity.deleteMany({userId: req.body.fieldId}, function (err) {});
                          res.status(200).json({
                              fieldName: req.body.fieldName,
                              fieldId: req.body.fieldId
                          });
                        }
                    });
                    break;
                default:
                    res.status(500).json({
                        message: "Invalid type provided"
                    });
            }
            break;
        default:
            res.status(500).json({
                message: "Invalid action provided"
            });
    }
    //next();
});

/*
router.use(function(request, response, next) {
  if(request.method == "OPTIONS") {
    next();
  } else {
    console.log("After!");
    next();
  }
});
*/

module.exports = router;
