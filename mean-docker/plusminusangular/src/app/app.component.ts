import { Component, OnInit } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { HttpHandler, HttpInterceptor, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {TokenStorage} from './token.storage';
import { TokenInterceptor } from './token.interceptor';

// Import rxjs map operator
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ]
})

export class AppComponent implements OnInit {
  title = 'app works!';

  // Link to our api, pointing to localhost
  API = 'http://' + location.hostname + ':3000';

  webToken = undefined;


  // Declare empty list of people
  people: Object = [];
  activities: Object = [];
  weights: Object = [];

  constructor(public tokenStorage: TokenStorage, private http: HttpClient) {}

  // Angular 2 Life Cycle event when component has been initialized
  ngOnInit() {
    this.getAllPeople(true);
  }

  // Get the weights for a user
  getWeightsForUser(userIdField) {
    var userId = userIdField.value;
    this.http.get(`${this.API}/weights/user/${userId}`)
        .map(res => res)
        .subscribe(weights => {
            console.log(weights);
            userIdField.value = "";
        })
  }

  loginUser(emailField, passwordField) {
    var email = emailField.value;
    var password = passwordField.value;
    this.http.post(`${this.API}/authenticate`, {email, password}, { observe: 'response' })
      .subscribe((response) => {
        console.log("Checking token storage");
        console.log(this.tokenStorage.getToken());
        if(this.tokenStorage.getToken() && this.tokenStorage.getToken() != "null") {
          emailField.value = "";
          passwordField.value = "";
          alert("Logged in!");
        } else {
          alert("Invalid email or password");
        }
      })
  }

  getToken() {
    return this.webToken;
  }

  // Add one person to the API
  addPerson(emailField, firstNameField, lastNameField, passwordField) {
    var email = emailField.value;
    var firstName = firstNameField.value;
    var lastName = lastNameField.value;
    var password = passwordField.value;
    this.http.post(`${this.API}/users`, {email, firstName, lastName, password})
      .map(res => res)
      .subscribe(() => {
        emailField.value = "";
        firstNameField.value = "";
        lastNameField.value = "";
        passwordField.value = "";
        this.getAllPeople();
      })
  }

    // Add an activity to the API
    addActivity(userIdField, dateTimeField, descriptionField, weightValueField) {
      var userId = userIdField.value;
      var dateTime = dateTimeField.value;
      var description = descriptionField.value;
      var weightValue = weightValueField.value;
        this.http.post(`${this.API}/activities`, {userId, dateTime, description, weightValue})
            .map(res => res)
            .subscribe(() => {
              userIdField.value = "";
              dateTimeField.value = "";
              descriptionField.value = "";
              weightValueField.value = "";
                this.getAllActivities();
        })
    }

    addWeight(userIdField, dateTimeField, weightField) {
        var userId = userIdField.value;
        var dateTime = dateTimeField.value;
        var weight = weightField.value;
        this.http.post(`${this.API}/weights`, {userId, dateTime, weight})
            .map(res => res)
            .subscribe(() => {
                userIdField.value = "";
                dateTimeField.value = "";
                weightField.value = "";
                this.getAllWeights();
            })
    }

    // Delete the provided field based on the provided ID
    deleteField(fieldName, fieldId) {
        this.http.post(`${this.API}/settings`, {"action": "delete", fieldName, fieldId})
            .map(res => res)
            .subscribe(() => {
                this.getAllPeople(true);
            })
    }

  // Get all users from the API
  getAllPeople(getAllActivities = false) {
    this.http.get(`${this.API}/users`)
      .subscribe(people => {
        console.log(people)
        this.people = people
        if(getAllActivities) {
          this.getAllActivities(true);
        }
      })
  }

  // Get all activities from the API
    getAllActivities(getAllWeights = false) {
        this.http.get(`${this.API}/activities`)
            .subscribe(activities => {
                console.log(activities)
                this.activities = activities
                if(getAllWeights) {
                  this.getAllWeights();
                }
        })
    }

    //Get all activities for a user
    getActivitiesForUser(userIdField) {
      var userId = userIdField.value;
        this.http.get(`${this.API}/activities/user/${userId}`)
            .subscribe(activities => {
                console.log(activities);
                userIdField.value = "";
            })
    }

    getAllWeights() {
        this.http.get(`${this.API}/weights`)
            .map(res => res)
            .subscribe(weights => {
                console.log(weights)
                this.weights = weights
        })
    }
}
